# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

categories = Category.create([{name: 'Design', color: '#00b300'}, {name: 'Technology', color: '#6b6b47'}, {name: 'World', color: '#990099'}, 
    {name: 'Culture', color: '#ff3300'}, {name: 'Business', color: '#000080'}, {name: 'Politics', color: '#664400'},
    {name: 'Opinion', color: '#cc66ff'}, {name: 'Science', color: '#660033'}, {name: 'Health', color: '#00cc00'}, {name: 'Style', color: '#ff0066'},
    {name: 'Other', color: '#1a001a'}])
