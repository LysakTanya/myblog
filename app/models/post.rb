class Post < ApplicationRecord
  belongs_to :category
  has_many :comments, dependent: :destroy
  validates :title, :summary, :body, :category, presence: true
end
