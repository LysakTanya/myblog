class Category < ApplicationRecord
    has_many :posts, dependent: :destroy
    validates :name, :color, presence: true
end
