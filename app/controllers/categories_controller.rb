class CategoriesController < ApplicationController
    before_action :set_category, only: [:show]

    def show
        @posts = Post.where(category_id: @category.id).order(id: :desc).paginate(page: params[:page], per_page: 6)
    end

    private
      def set_category
        @category = Category.find(params[:id])
      end
  
  end
  