class CommentsController < ApplicationController
    before_action :authenticate_user!
    before_action :set_post, only: [:create, :destroy]

    def create
        @comment = @post.comments.new(comments_params)
        @comment.user_id = current_user.id
        unless @comment.save
            redirect_to @post, alert: "Comment was not created."
        else 
            redirect_to @post, alert: "Comment was successfully created."
        end
    end

    def destroy
        @comment = @post.comments.find(params[:id])
        if (@comment.user_id == current_user.id || current_user.admin?)
            @comment.destroy
            redirect_to @post, alert: "You successfully deleted the comment."
        else 
            redirect_to @post, alert: "You cannot delete the comment."
        end
    end
    
    private
      def set_post
        @post = Post.find(params[:post_id])
      end

      def comments_params
        params.require(:comment).permit(:id, :post_id, :user_id, :content)
      end 
  end
  