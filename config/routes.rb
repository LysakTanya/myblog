Rails.application.routes.draw do
  devise_for :users, :controllers => { registrations: 'registrations' }
  root "posts#index"
  resources :posts, only: [:show, :index] do 
    resources :comments
  end
  resources :categories, only: :show

  namespace :admin do 
    resources :posts, except: [:index, :show]
  end
end
